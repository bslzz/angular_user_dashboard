import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs';
import { TitleResolver } from './core/resolvers/title.resolver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router, private route: ActivatedRoute, private titleResolver: TitleResolver){
    this.router.events.pipe(filter(event => event instanceof NavigationEnd), 
    map(() => this.titleResolver.getRoutePageTitle(this.route)))
    .subscribe(pageTitle => titleResolver.updateTitle(pageTitle));
  }
}
