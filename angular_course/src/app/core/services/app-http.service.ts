import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONSTANTS, environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AppHttpService {
  protected headers = {
    headers: {
      Authorization: `Bearer ${localStorage.getItem(CONSTANTS.mytoken)}`,
    },
  };

  constructor(private httpClient: HttpClient) {}

  public get<T>(relativeUrl: string) {
    const baseURL: string = environment.API_URL;
    const url = `${baseURL}/${relativeUrl}`;

    return this.httpClient.get<T>(url, this.headers);
  }

  public post<T>(relativeUrl: string, data: any) {
    const baseURL: string = environment.API_URL;
    const url = `${baseURL}/${relativeUrl}`;

    return this.httpClient.post<T>(url, data, this.headers);
  }
}
