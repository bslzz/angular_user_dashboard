import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import {
  ILogin,
  ILoginResponse,
  ILogout,
  ITokenData
} from 'src/app/core/models/auth.interface';
import { CONSTANTS, ENDPOINTS, environment } from 'src/environments/environment';
import { IUser } from '../models/user.interface';
import { AppHttpService } from './app-http.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  protected API: string = environment.API_URL;

  private _user!: IUser;

  private _token!: string;

  public get user() {
    return this._user;
  }

  public get token() {
    return this._token;
  }

  public set token(value: string) {
    this._token = value;
  }

  constructor(private appHttpClient: AppHttpService, private router: Router) {
    this._token = localStorage.getItem(CONSTANTS.mytoken) || ''
  }

  public login(user: ILogin): Observable<ILoginResponse> {
    return this.appHttpClient
      .post<ILoginResponse>(ENDPOINTS.login, user)
      .pipe(map((response) => response));
  }

  public loadUser(): Observable<IUser> {
    return this.appHttpClient
      .get<ITokenData>(ENDPOINTS.loadUser)
      .pipe(
        map((response) => {
          this._user = {
            ...response.data,
          };
          return this._user;
        })
      );
  }

  public logout(): Observable<ILogout> {
    return this.appHttpClient.get<ILogout>(ENDPOINTS.logout).pipe(map(response => response))
  }
}
