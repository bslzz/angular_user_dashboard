import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';
import { IRole } from '../models/user.interface';
import { AuthService } from '../services/auth.service';

@Directive({
  selector: '[authorize]',
})
export class AuthorizeDirective implements AfterViewInit {
  @Input('authorize') public allowRoles: IRole[];

  constructor(
    private elementRef: ElementRef,
    private authService: AuthService
  ) {
    this.allowRoles = [];
  }

  // used for inputs
  ngAfterViewInit(): void {
    const loggedInUser = this.authService.user.roleName;
    const userRole = this.allowRoles.findIndex((role) => role === loggedInUser);
    if (userRole === -1) {
      this.elementRef.nativeElement.remove();
    }
  }
}
