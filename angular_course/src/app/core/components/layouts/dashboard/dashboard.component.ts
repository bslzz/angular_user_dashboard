import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ILogout } from 'src/app/core/models/auth.interface';
import { AuthService } from 'src/app/core/services/auth.service';
import { CONSTANTS } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor(public authService: AuthService, public router: Router) {}

  ngOnInit(): void {}

  public logout() {
    window.confirm('Are you sure you want to logout?') &&
      this.authService.logout().subscribe((response: ILogout) => {
        if (response.id === 1 ) {
          localStorage.removeItem(CONSTANTS.mytoken);
          this.router.navigateByUrl('account/login');
        }
      });
  }
}
