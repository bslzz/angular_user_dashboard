import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../pages/account/login/login.component';
import { AccountComponent } from './components/layouts/account/account.component';
import { DashboardComponent } from './components/layouts/dashboard/dashboard.component';
import { AuthorizeDirective } from './directives/authorize.directive';
import { FilePathPipe } from './pipes/file-path.pipe';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared';

@NgModule({
  declarations: [
    LoginComponent,
    AccountComponent,
    DashboardComponent,
    FilePathPipe,
    AuthorizeDirective,
  ],

  imports: [
    HttpClientModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
})
export class CoreModule {}
