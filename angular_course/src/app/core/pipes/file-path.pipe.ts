import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'filePath',
})
export class FilePathPipe implements PipeTransform {
  transform(value: string, ...args: any[]): any {
    const baseAPI: string = environment.API_URL;
    const relativePath: string = value;
    return `${baseAPI}/${relativePath}`;
  }
}
