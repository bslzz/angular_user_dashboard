export interface IUser {
  firstName: string;
  lastName: string;
  profilePic: string;
  roleName: IRole;
  userCode: string;
}

export type IRole = 'Admin' | 'User' | 'Supervisor';
