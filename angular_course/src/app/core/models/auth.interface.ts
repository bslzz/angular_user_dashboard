import { IUser } from './user.interface';

export interface ILogin {
  username: string;
  password: string;
  rememberMe?: boolean;
}

export interface ILogout {
  data: number
  id: number
  message: string
}

export interface ILoginResponse {
  id: number;
  data: string;
  message: string;
}

export interface ITokenData {
  id: number;
  data: IUser;
  message: string;
}
