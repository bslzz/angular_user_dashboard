import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TitleResolver {
  private baseTitle: string;

  constructor(private title: Title) {
    this.baseTitle = title.getTitle();
  }
  
  public updateTitle(pageTitle: string) {
   this.title.setTitle(`${this.baseTitle} | ${pageTitle}`);
  }

  public getRoutePageTitle(route: ActivatedRoute): string {
    let child = route.firstChild;
    if (!child) {
      return route.snapshot.data['pageTitle'];
    }
    while (child.firstChild) {
      child = child.firstChild;
    }

    return child.snapshot.data['pageTitle'];

  }
}
