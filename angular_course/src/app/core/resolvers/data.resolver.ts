import { Injectable } from '@angular/core';
import {
  Resolve
} from '@angular/router';
import { Observable} from 'rxjs';
import { IUser } from '../models/user.interface';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class DataResolver implements Resolve<IUser> {
  constructor(private authService: AuthService) {}

  resolve(
  ): Observable<IUser> {
    return this.authService.loadUser()
  }
}
