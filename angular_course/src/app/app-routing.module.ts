import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './core/components/layouts/account/account.component';
import { DashboardComponent } from './core/components/layouts/dashboard/dashboard.component';
import { DataResolver } from './core/resolvers/data.resolver';
import { AuthGuardService } from './core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'account/login',
    pathMatch: 'full',
  },
  {
    path: 'account',
    component: AccountComponent,
    data: {
      pageTitle: 'Account Page',
    },
    children: [
      {
        path: 'login',
        data: {
          pageTitle: 'Login Page',
        },
        loadChildren: () =>
          import('./pages/account/login/login.module').then(
            (m) => m.LoginModule
          ),
      },
    ],
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuardService],
    resolve: {
      user: DataResolver,
    },
    data: {
      pageTitle: 'Dashboard Page',
    },

    children: [
      {
        path: '',
        data: { pageTitle: 'Dashboard Summary' },
        loadChildren: () =>
          import('./pages/dashboard/summary/summary.module').then(
            (m) => m.SummaryModule
          ),
      },
      {
        path: 'customer',
        data: {
          pageTitle: 'Customer Page',
        },
        loadChildren: () => import('./pages/dashboard/customer/customer.module').then(m => m.CustomerModule)
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
