import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { CONSTANTS } from 'src/environments/environment';
import { ILoginResponse } from '../../../core/models/auth.interface';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loginForm!: FormGroup;

  public apiResponse: ILoginResponse = {} as ILoginResponse;

  public pendingRequest: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    const username: string = localStorage.getItem(CONSTANTS.username) || '';

    this.loginForm = this.fb.group({
      userName: [username, Validators.required],
      password: ['', Validators.required],
      rememberMe: [
        localStorage.getItem(CONSTANTS.rememberMe) === 'true' ? true : false,
      ],
    });
  }

  ngOnInit(): void {}

  public postLoginForm(): void {
    this.pendingRequest = true;
    this.authService
      .login(this.loginForm.value)
      .subscribe((result: ILoginResponse) => {
          this.pendingRequest = false;
          this.apiResponse = result;

          if (this.loginForm.value.rememberMe) {
            localStorage.setItem(CONSTANTS.username, this.loginForm.value.userName);
            localStorage.setItem(CONSTANTS.rememberMe, this.loginForm.value.rememberMe);
          } else {
            localStorage.removeItem(CONSTANTS.username);
            localStorage.removeItem(CONSTANTS.rememberMe);
          }
          if (result && result.id) {
            this.authService.token = result.data
            localStorage.setItem(CONSTANTS.mytoken, result.data)
            this.router.navigate(['dashboard']);
          }
        })
      };

  public submitLoginForm() {
    if (this.loginForm.valid) {
      this.postLoginForm();
    }
  }
}
