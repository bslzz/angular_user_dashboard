import { NgModule } from '@angular/core';
import { AngularGridComponent } from './angular-grid.component';



@NgModule({
  declarations: [
    AngularGridComponent
  ],
  imports: [
  ],
  exports: [
    AngularGridComponent
  ]
})
export class AngularGridModule { }
