import { TestBed } from '@angular/core/testing';

import { AngularGridService } from './angular-grid.service';

describe('AngularGridService', () => {
  let service: AngularGridService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AngularGridService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
